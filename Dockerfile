FROM golang:1.19
WORKDIR /app
COPY go.mod go.sum ./
COPY .env ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /ms-user
EXPOSE 8080
CMD ["/ms-user"]