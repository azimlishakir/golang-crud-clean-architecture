package main

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"log"
	"ms-user/config/db"
	"ms-user/router"
	"net/http"
)

func main() {
	db.InitialMigration()
	router := myrouter.NewRooter(mux.NewRouter())
	handler := cors.New(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		MaxAge:           300,
		AllowCredentials: true,
		Debug:            true,
	}).Handler(router)

	log.Fatal(http.ListenAndServe(":8080", handler))

}
